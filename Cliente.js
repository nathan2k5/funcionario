const express = require('express');
const bodyParser = require('body-parser');
const {
    contentType
} = require('express/lib/response');
const app = express();
app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(bodyParser.json());
const port = 8000;

app.listen(port, () => {
    console.log("Projeto iniciado");
});

app.get('/clientes/filtro', (req, res) => {
    let source = req.query;
    let ret = "Dados enviados: " + source.nome + " " + source.sobrenome;
    res.send("{message " + ret + "}");
});

app.post('/clientes', (req, res) => {
    let dados = req.body;
    let headers_ = req.headers["access"];
    console.log("Valor Access: " + headers_);
    if (headers_ === "true") {
        let ret = "Dados Enviados: nome: " + dados.nome + " ";
        ret += "Sobrenome: " + dados.sobrenome + " ";
        ret += "Idade: " + dados.idade;
        res.send("{message: " + ret + "}");
    } else {
        res.send("{message: usuário não autorizado }");
    }
});