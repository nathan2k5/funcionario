const express = require('express');
const req = require('express/lib/request');
const bodyParser = require('body-parser');
const res = require('express/lib/response');
const app = express();
app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(bodyParser.json());
const port = 8000;

app.listen(port, () => {
    console.log("Projeto iniciado");
});

app.get('/funcionarios/filtro', (req, res) => {
    let source = req.query;
    let ret = "Dados enviados: " + source.nome + " " + source.sobrenome;
    res.send("{message " + ret + "}");
});

app.delete('/funcionarios/filtro/:id', (req, res) => {
    let headers_ = req.headers["access"]
    if (headers_ === "true") {
        let dado = req.params.id;
        res.send("{message: Funcionário de id: " + dado + ", " + "foi deletado}");
    } else {
        res.send("{message: Funcionário não foi deletado}");
    }
});

app.put('/funcionarios', (req, res) => {
    let dados = req.body;
    let headers_ = req.headers["access"];
    console.log("Valor Access: " + headers_);
    if (headers_ === "true") {
        let ret = "Dados atualizados: nome: " + dados.nome + " ";
        ret += "Sobrenome: " + dados.sobrenome + " ";
        ret += "Idade: " + dados.idade;
        res.send("{message: " + ret + "}");
    } else {
        res.send("{message: atualização não autorizada }");
    }
});